# cat-feeder

Keeping Snowball from getting fat and/or starving.

# Behavior
`feed.py ` Checks the scale to see if snowball has enough (27g) food. If he doesn't it puts out more. If he does, it logs the incident and moves on with its life.


# Installation

The project should run on a raspberry pi. You must have GPIO deb packages installed, and GPIO and bcm must be enabled.

1. Clone cat-feeder `git clone git@bitbucket.org:jjorissen/cat-feeder.git` (below steps assume home directory for a user named pi)
2. Setup pyenv
    * create desired virtualenv
    * `cd cat-feeder && pyenv local <virtualenvname>` to make sure your desired virtualenv is active whenever you are in that directory (requirement for gunicorn.service in this project to work)
    * `pip install -r requirements.txt`
3. Setup `feeder_rest_api` logging service
    * `cd cat-feeder/feeder_rest_api`
    * `python manage.py migrate`
    * `python manage.py createsuperuser`
    * Install to systemd
        ```
        cd cat-feeder/feeder_rest_api/feeder_rest_api
        sudo cp gunicorn.service /etc/systemd/system/gunicorn.service # install service file
        sudo systemctl daemon-reload # reload systemctl deamon so it picks up new service description
        sudo systemctl start gunicorn.service # start the service right now
        sudo systemctl status gunicorn.service # show running status
        # sudo systemctl enable gunicorn.service # enable as startup process
        ```
4. Setup `feed.py` on a regular schedule. I used [jobber](https://dshearer.github.io/jobber/doc/v1.4/). You can see the `feeder_api/.jobber` file in this project for a sample schedule
    * Install jobber and run jobbermaster
        ```
        cd cat-feeder/feeder_api
        sudo mkdir /run/jobber
        sudo cp jobber.service /etc/systemd/system/jobber.service # install service file
        sudo systemctl daemon-reload # reload systemctl deamon so it picks up new service description
        sudo systemctl start jobber.service # start the service right now
        sudo systemctl status jobber.service # show running status
        # sudo systemctl enable jobber.service # enable as startup process
        ```
    * Install and run jobs (see `feeder_api/.jobber` file for sample schedule)
        * Test using e.g. `jobber test MorningFeed` (can't do that with cron!)
    * Make edits as necessary
        * `jobber list`
        * change in `.jobber`
        * `jobber reload`
        * `jobber test`
        * rinse and repeat


