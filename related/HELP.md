This is where I found instructions on getting code for relay module: https://www.waveshare.com/wiki/Libraries_Installation_for_RPi . You are welcome to use the source code found in this repository. (MIT)

Something maybe slightly more helpful than those instructions (it's hard to tell): https://www.smart-prototyping.com/blog/Raspberry-Pi-Relay-Module-Tutorial


## WebIOPI

[These](http://www.knight-of-pi.org/webiopi-for-the-raspberry-pi-3/) seem to be the most up-to-date installation instructions available. 
```
cd ~
wget http://sourceforge.net/projects/webiopi/files/WebIOPi-0.7.1.tar.gz
tar xvzf WebIOPi-0.7.1.tar.gz
cd WebIOPi-0.7.1
wget https://raw.githubusercontent.com/doublebind/raspi/master/webiopi-pi2bplus.patch
patch -p1 -i webiopi-pi2bplus.patch
sudo ./setup.sh
```

I have manually patched a file in this repository so that it will work with python3.7. 

## [Relay Board Demo Download](https://www.smart-prototyping.com/image/data/2_components/Raspberry%20Pi/101790%20RPi%20Relay%20Board/RPi_Relay_Board.tar.gz)


## [HX711 Scale Tutorial](https://tutorials-raspberrypi.com/digital-raspberry-pi-scale-weight-sensor-hx711/)

Code:
`git clone https://github.com/tatobari/hx711py`



