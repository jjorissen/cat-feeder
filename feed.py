import logging

import sys
import time
import RPi.GPIO as GPIO

sys.path.insert(0, 'feeder_api')
from feeder.constants import *
from feeder.feed_him import feed_snowball

formatter = logging.Formatter('time=%(asctime)s name=%(name)s level=%(levelname)s message=%(message)s')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('feed-him.log')
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

logger.addHandler(handler)

if __name__ == "__main__":
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(FEEDER_CHANNEL, GPIO.OUT) 
    # turn it off immediately so first output is controlled by feeder_api
    GPIO.output(FEEDER_CHANNEL, GPIO.LOW)

    try:
        logger.info("channel open")
        feed_snowball(TARGET_READOUT)
    except (Exception, BaseException) as e:
        print(e)
        pass 
    finally:
        GPIO.output(FEEDER_CHANNEL, GPIO.LOW) # okay make sure it's off
        GPIO.cleanup()