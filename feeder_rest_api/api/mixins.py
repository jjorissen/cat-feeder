from django.db import models
from django.utils.timezone import now


class TimestampedMixin(models.Model):
    created_date = models.DateTimeField(default=now)
    updated_date = models.DateTimeField(default=now)

    class Meta:
        abstract = True

