from rest_framework import serializers, viewsets

from . models import FeedLog, FeedingError


class FeedLogSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = FeedLog
        fields = '__all__'


class FeedingErrorSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = FeedingError
        fields = '__all__'


class FeedLogViewSet(viewsets.ModelViewSet):
    """
    Provides CRUD capabilities to the `FeedLog` model.
    """
    queryset = FeedLog.objects.all()
    serializer_class = FeedLogSerializer

    class Meta:
        model = FeedLog


class FeedingErrorViewSet(viewsets.ModelViewSet):
    """
    Provides CRUD capabilities to the `FeedingError` model.
    """
    queryset = FeedingError.objects.all()
    serializer_class = FeedingErrorSerializer

    class Meta:
        model = FeedingError
