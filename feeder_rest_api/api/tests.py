from django.test import TestCase, LiveServerTestCase
from django.db import models as django_models
from django.contrib import admin as django_admin
from django.urls import reverse

from . import models, admin, api


MODEL_TYPES = [cls for cls in models.__dict__.values() if isinstance(cls, type)]
FEEDER_REST_API_MODELS = []

for _ in MODEL_TYPES:
    if type(_) is type(django_models.Model) and 'mixin' not in _.__name__.lower():
        FEEDER_REST_API_MODELS.append(_)


class TestEndpointsImplemented(TestCase):

    def test_all_implemented(self):
        # check that every model is represented in viewsets and serializers of api.py
        viewset_types = [cls for cls in api.__dict__.values() if isinstance(cls, type)]
        viewset_models = []

        for v in viewset_types:
            meta = getattr(v, 'Meta', None)
            if meta is not None:
                model = getattr(meta, 'model', None)
                if model:
                    viewset_models.append(model)
        for m in FEEDER_REST_API_MODELS:
            self.assertTrue(m in viewset_models,
                            f'Not all feeder_rest_api endpoints implemented; {m} endpoint not implemented')


class TestAdminImplemented(TestCase):

    def test_all_implemented(self):
        admin_types = [cls for cls in admin.__dict__.values() if isinstance(cls, type)]
        admin_models = []

        for a in admin_types:
            model = getattr(a, 'model', None)
            if model:
                admin_models.append(model)

        for m in FEEDER_REST_API_MODELS:
            self.assertTrue(m in admin_models,
                            f'Not all feeder_rest_api admin pages are implemented; {m} page not implemented')

    def test_all_resgistered(self):
        for m in FEEDER_REST_API_MODELS:
            self.assertIsNotNone(django_admin.site._registry.get(m, None),
                                 f'Not all feeder_rest_api admin pages are registered; {m} page not registered')


class TestURLSRouted(TestCase):

    def test_all_models_have_endpoint(self):
        for app_model in FEEDER_REST_API_MODELS:
            model_name = app_model._meta.model_name
            url = reverse(f"feeder_rest_api:{model_name}-list")


class TestEndpointsFunction(LiveServerTestCase):

    def test_list_works(self):

        for app_model in FEEDER_REST_API_MODELS:
            model_name = app_model._meta.model_name
            url = reverse(f"feeder_rest_api:{model_name}-list")
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200, 'status code was not OK')
