from django.contrib import admin

from .models import FeedLog, FeedingError


class FeedLogAdmin(admin.ModelAdmin):
    model = FeedLog
    list_display = ('should_feed', 'scale_readout', 'target_weight', 'created_date')


class FeedingErrorAdmin(admin.ModelAdmin):
    model = FeedingError
    list_display = ('feed_incident', 'created_date')


admin.site.register(FeedLog, FeedLogAdmin)
admin.site.register(FeedingError, FeedingErrorAdmin)
