from django.conf import settings
import sys, os
sys.path.insert(0, os.path.join(settings.BASE_DIR, '../', 'feeder_api'))

from django.db import models
# will be using this model class over models.Model for the entire project

import feeder
from .mixins import TimestampedMixin


class FeedLog(TimestampedMixin, models.Model):
    feed_uuid = models.UUIDField()
    target_weight = models.FloatField()
    scale_readout = models.FloatField()
    should_feed = models.BooleanField()

    @property
    def scale_readout_ounces(self) -> int:
        scale_readout: int = int(self.scale_readout)
        return feeder.utils.convert_to_ounces(scale_readout)


class FeedingError(TimestampedMixin, models.Model):
    feed_incident = models.ForeignKey(FeedLog, null=True, blank=True, on_delete=models.CASCADE)
    traceback = models.TextField(null=False, blank=False)
