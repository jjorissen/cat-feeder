from rest_framework import routers

from .api import FeedLogViewSet, FeedingErrorViewSet

router = routers.DefaultRouter()

router.register(r'feedlogs', FeedLogViewSet)
router.register(r'feedingerrors', FeedingErrorViewSet)
