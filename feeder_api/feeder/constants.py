# very confused
RELAY_CHANNEL_1 = 26
RELAY_CHANNEL_2 = 20
RELAY_CHANNEL_3 = 21

FEEDER_CHANNEL = RELAY_CHANNEL_3

GRAMS_REFERENCE_UNIT = 359
EMPTY_READOUT = -104707.9 # average tare with dish
TARGET_READOUT = 10000 # average readout after adding food to dish

MAX_TIME_ELAPSED = 30 # seconds
