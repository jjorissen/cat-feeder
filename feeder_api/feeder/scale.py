from . import constants
from hx711 import HX711

class Scale(HX711):
    def tare_A(self, times=15):
        # Backup REFERENCE_UNIT value
        backupReferenceUnit = self.get_reference_unit_A()
        self.set_reference_unit_A(1)
        
        # we cannot allow the scale to Tare itself because the scale is not guaranteed to be empty.
        # we will just have to monitor this for drift over time.
        value = constants.EMPTY_READOUT

        if self.DEBUG_PRINTING:
            print("Tare A value:", value)
        
        self.set_offset_A(value)

        # Restore the reference unit, now that we've got our offset.
        self.set_reference_unit_A(backupReferenceUnit)

        return value