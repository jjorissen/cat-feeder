from . import constants

def convert_to_ounces(scale_readout: int) -> int:
    """
    Takes a readout from the scale and converts it to ounces
    :param scale_readout: input from scale
    :return: ounces
    """
    return scale_readout / constants.GRAMS_REFERENCE_UNIT