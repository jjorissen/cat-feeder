##################################################

#           P26 ----> Relay_Ch1
#			P20 ----> Relay_Ch2
#			P21 ----> Relay_Ch3

##################################################
#!/usr/bin/python
# -*- coding:utf-8 -*-

import logging
try:
    import RPi.GPIO as GPIO
except:
    logging.error("could not import GPIO library.")
import time
import traceback

from . constants import *


def channel_one():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(FEEDER_CHANNEL, GPIO.OUT)
    #Control the Channel 1
    try:
        GPIO.output(FEEDER_CHANNEL, GPIO.LOW) # low means on for this relay
        print("Channel 1:The Common Contact is access to the Normal Open Contact!")
        time.sleep(3)
    except (Exception, BaseException) as e:
        traceback_str = ''.join(traceback.format_tb(e.__traceback__))
        print(traceback_str)
    finally:
        GPIO.cleanup()


def startup_check():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(RELAY_CHANNEL_1, GPIO.OUT)
    GPIO.setup(RELAY_CHANNEL_2, GPIO.OUT)
    GPIO.setup(RELAY_CHANNEL_3, GPIO.OUT)
    try:
        for i in range(3):
            # Control the Channel 1
            GPIO.output(RELAY_CHANNEL_1, GPIO.LOW)
            print("Channel 1:The Common Contact is access to the Normal Open Contact!")
            time.sleep(0.5)

            GPIO.output(RELAY_CHANNEL_1, GPIO.HIGH)
            print("Channel 1:The Common Contact is access to the Normal Closed Contact!\n")
            time.sleep(0.5)

            # Control the Channel 2
            GPIO.output(RELAY_CHANNEL_2, GPIO.LOW)
            print("Channel 2:The Common Contact is access to the Normal Open Contact!")
            time.sleep(0.5)

            GPIO.output(RELAY_CHANNEL_2, GPIO.HIGH)
            print("Channel 2:The Common Contact is access to the Normal Closed Contact!\n")
            time.sleep(0.5)

            # Control the Channel 3
            GPIO.output(RELAY_CHANNEL_3, GPIO.LOW)
            print("Channel 3:The Common Contact is access to the Normal Open Contact!")
            time.sleep(0.5)

            GPIO.output(RELAY_CHANNEL_3, GPIO.HIGH)
            print("Channel 3:The Common Contact is access to the Normal Closed Contact!\n")
            time.sleep(0.5)

    except (Exception, BaseException) as e:
        print(e)
        traceback_str = ''.join(traceback.format_tb(e.__traceback__))
        print(traceback_str)
    finally:
        GPIO.cleanup()



