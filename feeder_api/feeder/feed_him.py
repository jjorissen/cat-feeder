"""
feed_snowball uses very simple logic to determine whether or not he should be fed; If the weight_readout of the scale
is lower than than the target_weight, he gets fed. It is up to the caller to determine when (and how often)
to call feed_snowball
"""

import logging

import uuid
import time
import datetime
import traceback
import requests
import RPi.GPIO as GPIO

from typing import Any, Union

from pipeline import execution_pipeline
from pipeline.validators import ExceptionHandler

from . constants import *
from . utils import convert_to_ounces
from . relay import channel_one, startup_check
from . scale import Scale

formatter = logging.Formatter('time=%(asctime)s name=%(name)s level=%(levelname)s message=%(message)s')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('feed-him.log')
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

logger.addHandler(handler)


class FeedNotFinished(BaseException):
    pass


def record_error_and_notify(e: Union[Exception, BaseException], response: int):
    traceback_str = ''.join(traceback.format_tb(e.__traceback__))
    # we just want the page info from the paginator
    feed_list = requests.get('http://127.0.0.1:8000/api/feedlogs/?limit=1')
    feed_list_object = feed_list.json()
    last_page = feed_list_object['links']['last']
    # okay, goto last page
    last_feed_page_results = requests.get(last_page).json()['results']
    last_feed_url = last_feed_page_results[-1]['url'] if len(last_feed_page_results) != 0 else None

    feed_error_data = {
        "traceback": f'{e}: \n\n\n{traceback_str}',
        # here we are assuming that a feed incident is always logged
        # and that our last available feed_incident will be the one that caused
        # the problem.
        # this is a decent assumption...
        "feed_incident": last_feed_url
    }
    requests.post('http://127.0.0.1:8000/api/feedingerrors/', data=feed_error_data)

    logger.error(traceback_str)
    raise e


error_handlers = [
    ExceptionHandler(handler=record_error_and_notify, exception_classes=(Exception, BaseException)),
]


def _hes_hungry(scale_readout: int, target_readout: int) -> bool:
    """
    Takes a readout from the scale and determines whether or not the cat should be fed
    :param weight_readout:
    :param target_weight:
    :return:
    """

    return target_readout > scale_readout


# error handler will log exceptions to the API and then raise them
@execution_pipeline(error=error_handlers)
def feed_snowball(target_readout: int) -> int:
    """
    Feeds the cat.
    :return: 0 for success, other for failure
    """
    UUID = uuid.uuid4()
    start_time = time.time()

    # initialize scale
    scale = Scale(5, 6)
    scale.set_reading_format("MSB", "MSB")
    scale.set_reference_unit(1)
    scale.reset()
    tare = scale.tare()
    # set and read scale zero-value readout
    logger.info(f"Tare {tare}")
    scale_readout = max(0, scale.get_weight(5)) # abuse!
    weight_readout: int = convert_to_ounces(scale_readout)
    logger.info(f"The food dish currently weighs {weight_readout} ounces.")
    logger.info(f"should_feed_him: {_hes_hungry(scale_readout, target_readout)}")
    if _hes_hungry(scale_readout, target_readout):
        try:
            GPIO.output(FEEDER_CHANNEL, GPIO.HIGH) # okay turn it on
            logger.info("channel open")
            latest_time = time.time()
            while _hes_hungry(scale_readout, target_readout) and (latest_time - start_time < MAX_TIME_ELAPSED):
                scale.power_down()
                feed_data = {
                    "feed_uuid": UUID,
                    "target_weight": convert_to_ounces(target_readout),
                    "scale_readout": convert_to_ounces(scale_readout),
                    "should_feed": _hes_hungry(scale_readout, target_readout),
                }
                print(feed_data)
                requests.post('http://127.0.0.1:8000/api/feedlogs/', data=feed_data)
                time.sleep(0.5)
                scale.power_up()
                scale_readout = max(0, scale.get_weight(5))
                latest_time = time.time()
            else:
                scale_readout = max(0, scale.get_weight(5))
                if latest_time - start_time > MAX_TIME_ELAPSED:
                    raise FeedNotFinished(f'Could not feed Snowball in allotted time '
                                          f'(elapsed: {latest_time - start_time}, max {MAX_TIME_ELAPSED})'
                                          f'maybe something is wrong with the feeder?')
                else:
                    logger.info(f"Fed in {latest_time - start_time} seconds at {datetime.datetime.now()}.")
        except (Exception, BaseException) as e:
            traceback_str = ''.join(traceback.format_tb(e.__traceback__))
            logger.error(traceback_str)
            raise e
        finally:
            GPIO.output(FEEDER_CHANNEL, GPIO.LOW) # okay turn it off
            feed_data = {
                "feed_uuid": UUID,
                "target_weight": convert_to_ounces(target_readout),
                "scale_readout": convert_to_ounces(scale_readout),
                "should_feed": _hes_hungry(scale_readout, target_readout),
            }
            requests.post('http://127.0.0.1:8000/api/feedlogs/', data=feed_data)
    else:
        logger.warning(f"Guess he wasn't hungry at {datetime.datetime.now()}")
        feed_data = {
            "feed_uuid": UUID,
            "target_weight": convert_to_ounces(target_readout),
            "scale_readout": convert_to_ounces(scale_readout),
            "should_feed": _hes_hungry(scale_readout, target_readout),
        }
        requests.post('http://127.0.0.1:8000/api/feedlogs/', data=feed_data)

    return 0
