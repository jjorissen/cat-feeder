from setuptools import setup

setup(name='feeder_api',
      version='0.1',
      description='feeder sub-app',
      url='',
      author='me',
      author_email='flyingcircus@example.com',
      license='MIT',
      packages=['feeder'],
      zip_safe=False)