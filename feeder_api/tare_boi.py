"""
Unforuntely, because our payload will never be zerod out, we cannot allow the scale to use a tare for our purposes. Instead, we will use the program to 
aggregate tare values over a long period of time. God speed.
"""

import time
import sys
import logging

logger = logging.getLogger(__name__)

import RPi.GPIO as GPIO
from hx711 import HX711

stream_handler = logging.StreamHandler()
file_handler = logging.FileHandler('tare.log')

logger.setLevel(logging.DEBUG)
stream_handler.setLevel(logging.DEBUG)
file_handler.setLevel(logging.DEBUG)

log_format = logging.Formatter('%(message)s')
stream_handler.setFormatter(log_format)
file_handler.setFormatter(log_format)

logger.addHandler(stream_handler)
logger.addHandler(file_handler)

def cleanAndExit():
    print("Cleaning...")
    GPIO.cleanup()    
    print("Bye!")
    sys.exit()

hx = HX711(5, 6)

hx.set_reading_format("MSB", "MSB")

hx.set_reference_unit(1)

while True:
    try:
        hx.reset()
        tare = hx.tare()
        logger.info(f"{tare},")
        hx.power_down()
        time.sleep(60) # just do it every minute dawg, don't even trip
        hx.power_up()
        time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        cleanAndExit()
